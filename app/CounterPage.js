/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button
} from 'react-native';


class Counter extends React.Component {

    static navigationOptions = {
        title: 'Counter Page',
      };

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { counter: isNaN(parseInt(this.props.navigation.getParam('init_counter', "0"))) ? 0 : parseInt(this.props.navigation.getParam('init_counter', "0")) };
        }

    handleCounter(param){
        this.setState({
            counter : param == "+" ? this.state.counter + 1 : this.state.counter - 1
        });

    }

    render() {
        return (
        <View style={styles.secContent}>
            <Text style={styles.secBody,{fontSize:17,fontWeight: "bold"}}>
                Counter
            </Text>
            
            <Text style={styles.secBody}>
                {this.state.counter}
            </Text>

            <View style={{flexDirection: 'row',justifyContent: 'space-between',}}>
                <Button style={{margin:10}} onPress={() => this.handleCounter("-")} title="-"/>
                <Button style={{margin:10}} onPress={() => this.handleCounter("+")} title="+"/>
            </View>
            

        </View>
        );
    }
}


const styles = StyleSheet.create({
    secBody : {
        textAlign : "center",
        margin:10,
    },
    secContent :{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'

    }
  });
  


export default Counter;
