import React, {Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Home from './Home';
import Counter from './CounterPage';
import Book from './BooksPage';

  const AppNavigator = createStackNavigator(
    {
      Home: Home,
      Counter : Counter,
      Book : Book
    },
    {
      initialRouteName: 'Book',
    }
  );
  
  export default createAppContainer(AppNavigator);