/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TextInput
} from 'react-native';


class Home extends React.Component {

  static navigationOptions = {
    title: 'Home',
  };
  
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { counter: 0 };
    }

  onChangeCounter(param){
    this.setState({
      counter: param
    })
  }

  render() {
    return (
      <View style={styles.secContent}>
          <Text style={styles.secBody,{fontSize:17,fontWeight: "bold"}}>
              Welcome to CodingID Hive Prominence
          </Text>
          
          <Text style={styles.secBody}>
              Init Counter
          </Text>

          <TextInput
            style={{marginBottom:10,height:40,borderColor: 'gray', borderWidth: 1 }}
            onChangeText={text => this.onChangeCounter(text)}
            //value={this.state.counter}
          />

          <Button
            title="Goto Counter Page"
            onPress={() => this.props.navigation.navigate('Counter',{
              init_counter : this.state.counter
            })}
            >
          </Button>

          <Button
            title="Goto Books Page"
            style={{margin:10}}
            onPress={() => this.props.navigation.navigate('Book')}
            >
          </Button>

          
      </View>
    );
  }
}


const styles = StyleSheet.create({
    secBody : {
        textAlign : "center",
        margin:10,
    },
    secContent :{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'

    }
  });
  


export default Home;
//export default createAppContainer(AppNavigator);
