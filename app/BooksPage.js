import React, {Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList
} from 'react-native';

import axios from 'axios';
import { thisTypeAnnotation } from '@babel/types';

//Import Component
import ComBook from './component/ComBook';

class Book extends React.Component {

    constructor(props) {
        super(props);
        
          this.state = { 
            data: [], //for list books
            current_page : 1, //for pagination 
            num_books : 5, //for maks item in a page
            api_key : "AIzaSyDOJKbIdHyjnd-VYGWJn-lXMIruyarKrZ0"
          };
        }

    componentDidMount() {
        this.callAPI()
    }

    //callAPI used for get data from API based current page and number of books that return from API
    callAPI(){
      console.log("tes api")
      let start_index = this.state.num_books * (this.state.current_page - 1)
      console.log(this.state.current_page)
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=flowers&key=` + this.state.api_key + `&startIndex=` + start_index + `&maxResults=`+this.state.num_books)
            .then(res => {
              console.log(res);
              this.setState({
                  data: [...this.state.data, ...res.data.items], //add array into array, key : Immutability Array
                  current_page: this.state.current_page + 1
              })
            
            })
            .catch(err => {
              console.log("error api")
              console.log(err)
            })
    }

    //endListBook called when user reach end of list
    endListBook=()=>{
      console.log("End Reached")
      this.callAPI()
    }

  render() {
    return (
      <View style={styles.secContent}>
          <Text style={{textAlign:"center", fontSize:20, padding:10}}>Google Books</Text>
          {this.state.data != null ? 
            <FlatList
            extraData={this.state.data}
            data={this.state.data != null ? this.state.data : []}
            renderItem={({item}) => 
            <ComBook 
              title = {item.volumeInfo.title}
              listAuthors = {item.volumeInfo.authors}
              image = {item.volumeInfo.imageLinks.thumbnail}
            />
            }
            keyExtractor={(item, index) => item.id + index}
            onEndReached={this.endListBook}
            onEndReachedThreshold={0.2}
          /> : <Text>Loading Data...</Text>
            }
      </View>
    );
  }
}


const styles = StyleSheet.create({
    secBody : {
        textAlign : "center",
        margin:10,
    },
    secContent :{
      flex: 1,

    }
  });
  


export default Book;