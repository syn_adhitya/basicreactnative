import React, { Fragment } from 'react';
import {
    View,
    Text,
    Image,
} from 'react-native';

export default class ComBook extends React.Component {
    listAuthor(list) {
        if (list == null) {
            return "No Author";
        }
        console.log("Log 1");
        console.log(list);
        console.log(list.length);
        let authors = "";
        for (let i = 0; i < list.length; i++) {
            authors += i < (list.length - 1) ? list[i] + ", " : list[i]
        }
        return authors;
    }

    render() {
        return (
            <View style={{ padding: 10, borderBottomWidth: 5, borderRightColor: "grey", flexDirection:"row" }}>
                <Image
                    style={{flex:1, width: 100, height: 100, resizeMode:"stretch" }}
                    source={{ uri: 'https://facebook.github.io/react-native/img/tiny_logo.png'}}//this.props.image }}
                />
                <View style={{flex:3,marginLeft:10, marginEnd:10}}>
                    <Text style={{fontWeight:"bold", flexShrink: 1}}>{this.props.title}</Text>
                    <Text style={{flexShrink: 1}}>Author : {
                        this.listAuthor(this.props.listAuthors)
                    }</Text>
                </View>
                
            </View>

        )
    }
}